npm run go

rm -fr ./plugins-release-brain
mkdir ./plugins-release-brain
cp -r ./plugins-release/plugins/commander ./plugins-release-brain
cp -r ./plugins-release/plugins/commandpalette ./plugins-release-brain
cp -r ./plugins-release/plugins/ContextPlugin ./plugins-release-brain
echo skip CPL-Repo
cp -r ./plugins-release/plugins/date-filters ./plugins-release-brain
echo skip doc
cp -r ./plugins-release/plugins/echarts ./plugins-release-brain
echo skip echarts-gl
echo skip echarts-graph-modularity
echo skip  echarts-liquidfill
echo skip  echarts-stat
cp -r ./plugins-release/plugins/editor-autolist ./plugins-release-brain
cp -r ./plugins-release/plugins/file-uploads ./plugins-release-brain
cp -r ./plugins-release/plugins/file-uploads-PUT ./plugins-release-brain
cp -r ./plugins-release/plugins/link-to-tabs ./plugins-release-brain
cp -r ./plugins-release/plugins/locator ./plugins-release-brain
cp -r ./plugins-release/plugins/mob ./plugins-release-brain
cp -r ./plugins-release/plugins/node-files-PUT-support ./plugins-release-brain
cp -r ./plugins-release/plugins/plainrevs ./plugins-release-brain
echo skip  plugin-name
echo skip  prevent-edit
cp -r ./plugins-release/plugins/preview ./plugins-release-brain
cp -r ./plugins-release/plugins/projectify ./plugins-release-brain
cp -r ./plugins-release/plugins/relink ./plugins-release-brain
cp -r ./plugins-release/plugins/relink-fieldnames ./plugins-release-brain
cp -r ./plugins-release/plugins/relink-markdown ./plugins-release-brain
cp -r ./plugins-release/plugins/relink-titles ./plugins-release-brain
cp -r ./plugins-release/plugins/relink-variables ./plugins-release-brain
cp -r ./plugins-release/plugins/section ./plugins-release-brain
cp -r ./plugins-release/plugins/shiraz ./plugins-release-brain
cp -r ./plugins-release/plugins/shiraz-callout ./plugins-release-brain
cp -r ./plugins-release/plugins/shiraz-formatter ./plugins-release-brain
echo skip  tables
echo skip  task-manager
cp -r ./plugins-release/plugins/tiddlywiki-cmp ./plugins-release-brain
cp -r ./plugins-release/plugins/tiddlywiki-cmp-emoji ./plugins-release-brain
cp -r ./plugins-release/plugins/tiddlywiki-codemirror-6 ./plugins-release-brain
cp -r ./plugins-release/plugins/tiddlywiki-snippets ./plugins-release-brain
cp -r ./plugins-release/plugins/trashbin ./plugins-release-brain
cp -r ./plugins-release/plugins/tw5-checklist ./plugins-release-brain
cp -r ./plugins-release/plugins/tw5-keyboard-navigation ./plugins-release-brain
cp -r ./plugins-release/plugins/tw-calendar ./plugins-release-brain
cp -r ./plugins-release/plugins/zbase ./plugins-release-brain
echo complete copied